#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include "mem.h"

void log_process_totals(struct process_totals *process) {
    pid_t pid = process->pid;
    struct pid_totals totals = process->totals;
    if (totals.maps > 0) {
        printf("\n%d name %s\n", pid, process->name);
        printf("%d path %s\n", pid, process->path);
        printf("%d hash ", pid);
        int i;
        for (i=0; i<MD5_DIGEST_LENGTH; i++) 
            printf("%02x", process->hash[i]);
        printf("\n");
        printf("%d maps %d\n", pid, totals.maps);
        for (i=0; i<totals.maps; i++)
            printf("%s|", totals.addrs[i]);
        printf("\n");
        printf("%d vss %d\n", pid, totals.size);
        printf("%d rss %d\n", pid, totals.rss);
        printf("%d pss %d\n", pid, totals.pss);
        printf("%d uss %d\n", pid, totals.uss);
        printf("%d shared_clean %d\n", pid, totals.shared_clean);
        printf("%d shared_dirty %d\n", pid, totals.shared_dirty);
        printf("%d private_clean %d\n", pid, totals.private_clean);
        printf("%d private_dirty %d\n", pid, totals.private_dirty);
        printf("%d referenced %d\n", pid, totals.referenced);
        printf("%d swap %d\n", pid, totals.swap);
    }
}

int populate_pid_totals(pid_t pid, struct pid_totals *pid_totals) {
    char *filename;
    if (asprintf(&filename, "/proc/%d/smaps", pid) < 0) {
        fprintf(stderr, "filename error: %s\n", strerror(errno));
        goto err_fname;
    }
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        fprintf(stderr, "fopen %s failed: %s\n", filename, strerror(errno));
        goto err_open;
    }
    free(filename);
    
    pid_totals->size = 0;
    pid_totals->rss = 0;
    pid_totals->pss = 0;
    pid_totals->shared_clean = 0;
    pid_totals->shared_dirty = 0;
    pid_totals->private_clean = 0;
    pid_totals->private_dirty = 0;
    pid_totals->referenced = 0;
    pid_totals->swap = 0;
    pid_totals->uss = 0;
    pid_totals->maps = 0;
    int init_maps = 10;
    pid_totals->addrs = malloc(sizeof(char*) * init_maps);

    char line[256];
    while (fgets(line, sizeof(line), f)) {
        char *c;
        c = strtok(line, " ");
        while (c != NULL) {
            if (strchr(c, '-') == NULL) {   // Xxx: yy kB
                if (strcmp(c, "Size:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->size += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Rss:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->rss += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Pss:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->pss += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Shared_Clean:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->shared_clean += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Shared_Dirty:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->shared_dirty += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Private_Clean:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->private_clean += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Private_Dirty:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->private_dirty += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Referenced:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->referenced += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                else if (strcmp(c, "Swap:") == 0) {
                    c = strtok(NULL, " ");
                    pid_totals->swap += strtol(c, NULL, 10); 
                    c = strtok(NULL, " ");
                }
                c = strtok(NULL, " ");
            }
            else {
                if (pid_totals->maps >= init_maps) {
                    init_maps += 10;
                    pid_totals->addrs = (char **) realloc(pid_totals->addrs, sizeof(char*) * init_maps);
                }
                pid_totals->addrs[pid_totals->maps] = malloc(sizeof(char) * 36);
                strncpy(pid_totals->addrs[pid_totals->maps], c, strlen(c)); 
                pid_totals->addrs[pid_totals->maps][strlen(c)] = '\0';
                pid_totals->maps++;
                break;
            }
        }
    }
    pid_totals->uss = pid_totals->private_clean + pid_totals->private_dirty;
    fclose(f);
    return 0;
err_parse:
    fclose(f);
err_open:
    free(filename);
err_fname:
    return -1;
}

int get_pid_name(pid_t pid, struct process_totals *p) {
    char *filename;
    if (asprintf(&filename, "/proc/%d/stat", pid) < 0) {
        fprintf(stderr, "filename error: %s\n", strerror(errno));
        goto err_fname;
    }
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        fprintf(stderr, "fopen %s failed: %s\n", filename, strerror(errno));
        goto err_open;
    }
    free(filename);
    
    char line[256];
    if (fgets(line, sizeof(line), f)) {
        char *start = strchr(line, '(');
        char *end = strchr(line, ')');
        strncpy(p->name, start+1, end-start-1);
        p->name[end-start-1] = '\0';
    }
    return 0;
err_parse:
    fclose(f);
err_open:
    free(filename);
err_fname:
    return -1;
}

int get_file_hash(struct process_totals *p) {
    char *filename = p->path;
    FILE *f = fopen(filename, "rb");
    if (f == NULL) {
        fprintf(stderr, "fopen %s failed: %s\n", filename, strerror(errno));
        goto err_open;
    }

    int bytes;
    unsigned char data[1024];
    unsigned char c[MD5_DIGEST_LENGTH];
    MD5_CTX mdctx;
    MD5_Init(&mdctx);
    while ((bytes = fread(data, 1, 1024, f)) != 0) {
        MD5_Update(&mdctx, data, bytes);
    }
    MD5_Final(c, &mdctx);

    memcpy(p->hash, c, MD5_DIGEST_LENGTH);
    
    return 0;
err_parse:
    fclose(f);
err_open:
    return -1;
}

int get_pid_path(pid_t pid, struct process_totals *p) {
    char *filename;
    if (asprintf(&filename, "/proc/%d/exe", pid) < 0) {
        fprintf(stderr, "filename error: %s\n", strerror(errno));
        goto err_fname;
    }
    int n;
    if ((n = readlink(filename, p->path, PATH_MAX)) < 0) {
        fprintf(stderr, "readlink %s error: %s\n", filename, strerror(errno));
        goto err_open;
    }
    p->path[n] = '\0';

    if (get_file_hash(p) < 0) {
        fprintf(stderr, "get %s file hash error: %s\n", p->path, strerror(errno));
        goto err_open;
    }

    free(filename);
    return 0;
err_open:
    free(filename);
err_fname:
    return -1;
}

void populate_process_totals(pid_t pid, struct mem_table *t) {
    struct process_totals *p;
    if (t->size == 0) {
        t->last = malloc(sizeof(struct process_totals));
        p = t->last;
    }
    else {
        p = malloc(sizeof(struct process_totals));
    }

    p->pid = pid;
    if (get_pid_name(p->pid, p) < 0 || 
            get_pid_path(p->pid, p) < 0 ||
            populate_pid_totals(p->pid, &(p->totals)) < 0) {
        free(p);
        return;
    }
    p->next = NULL;

    log_process_totals(p);

    if (t->size == 0) {
        t->root = p;
    }
    else {
        t->last->next = p;
        t->last = p;
    }
    t->size++;
}

int snapshot_log(struct mem_table *t, char *filename) {
    if (filename == NULL || strlen(filename) < 1) {
        fprintf(stderr, "invalid filename\n");
        goto err_fname;
    }
    FILE *csv_fp = fopen(filename, "w+");
    if (csv_fp == NULL) {
        fprintf(stderr, "fopen csv failed: %s\n", strerror(errno));
        goto err_open;
    }
    time_t timestamp = time(NULL);

    fprintf(csv_fp, "timestamp,pid,name,path,hash,maps,addrs,vss,rss,pss,uss,shared_clean,shared_dirty,private_clean,private_dirty,referenced,swap\n");

    struct process_totals *p = t->root;
    int i;
    while (p != NULL) {
        if (p->totals.maps > 0) {
            fprintf(csv_fp, "%"PRIu64",%d,%s,%s,", (unsigned long long)timestamp, p->pid, p->name, p->path);
            for (i=0; i<MD5_DIGEST_LENGTH; i++) fprintf(csv_fp, "%02x", p->hash[i]);
            fprintf(csv_fp, ",%d,", p->totals.maps);
            for (i=0; i<p->totals.maps; i++) fprintf(csv_fp, "%s|", p->totals.addrs[i]);
            fprintf(csv_fp, ",%d,%d,%d,%d,", p->totals.size, p->totals.rss, p->totals.pss, p->totals.uss);
            fprintf(csv_fp, "%d,%d,%d,%d,", p->totals.shared_clean, p->totals.shared_dirty, p->totals.private_clean, p->totals.private_dirty);
            fprintf(csv_fp, "%d,%d\n", p->totals.referenced, p->totals.swap);
        }
        p = p->next;
    }

    fclose(csv_fp);
    return 0;
err_parse:
    fclose(csv_fp);
err_open:
err_fname:
    return -1;
}

int main() {
    struct mem_table table; 
    table.size = 0;
//get list of all pids
    DIR *proc;
    struct dirent *ent;
    proc = opendir("/proc");
    if (proc == NULL) {
        fprintf(stderr, "could not open /proc: %s\n", strerror(errno));
    }

//loop through pids and construct their process_totals
    while (ent = readdir(proc)) {
        if (! isdigit(*ent->d_name)) {
            continue;
        }
        pid_t pid = strtol(ent->d_name, NULL, 10);
        populate_process_totals(pid, &table);
    }

//test write a snapshot in csv format
    snapshot_log(&table, "test.csv");

    closedir(proc);
}
